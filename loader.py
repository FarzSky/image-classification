import os
from scipy import misc

class LoadedImage():
    """
    Represents a single loaded image. Contains three attributes: _filename which is original name of the image file
    with extension, _path which is relative path of the image and _bytes which is byte array representation of the
    image.
    """
    def __init__(self, filename, path, bytes):
        self._filename = filename
        self._path = path
        self._bytes = bytes

def load_as_rgb(dir, dont_load_bytes = False):
    """
    Loads all images in the given directory as an RGB image.
    """
    images = []

    for folder, subs, files in os.walk(dir):
        for filename in files:
            path = os.path.join(folder, filename)
            with open(path, 'r') as image_file:
                try:
                    im = misc.imread(path, mode='RGB') if not dont_load_bytes else None
                    images.append(LoadedImage(filename, path, im))
                    #print ("Image: " + path + " loaded.")
                except:
                    print("Warning: " + filename + " could not be opened as image.")

    return images
