from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
import sklearn.cluster as sc
import sklearn
import numpy as np

class Clusterer():


    def cluster(self, features):
        raise NotImplementedError("Not implemented.")


    def get_inertia(self):
        raise NotImplementedError("Not implemented.")


class KMeans(Clusterer):


    def __init__(self, components, max_iter=300, tol=0.0001):
        self._components = components
        self.__delegate = sklearn.cluster.KMeans(n_clusters=components, max_iter=max_iter, tol=tol)


    def cluster(self, features):
        return self.__delegate.fit_predict(features)

    def get_inertia(self):
        return self.__delegate.inertia_


class FuzzyKMeans(Clusterer):


    def __init__(self, components, fuzzyness = 2.0, max_iter=300, min_update_distance = 0.0001):
        self._components = components
        self._f = fuzzyness
        self._max_iter = max_iter
        self._eps = min_update_distance


    def cluster(self, features):
        if features is None or not features:
            return
        feature_length = len(features[0])
        self._centroids = []
        for i in range(0, self._components):
            self._centroids.append(features[np.random.randint(0, len(features))])

        print (len(self._centroids[0]))
        iter = 0
        total_update_distance = self._eps + 1.0
        mus = None
        while iter < self._max_iter and total_update_distance >= self._eps:
            dists = FuzzyKMeans.__get_dists(features, self._centroids)
            mus = FuzzyKMeans.__get_mus(features, self._centroids, self._f, dists)
            old_centroids = self._centroids
            self.__update_centroids(features, mus, self._f)
            total_update_distance = FuzzyKMeans.__get_total_update_distance(old_centroids, self._centroids)

        self.__inertia = 0.0
        print(len(self._centroids[0]))
        prediction = np.asarray([self.__get_label(mus, fi, features[fi]) for fi in range(0, len(features))])
        return prediction


    def __get_label(self, mus, fi, feature):
        if mus is None:
            return -1
        label = 0
        max_mus = mus[fi][0]
        for mi in range(1, self._components):
            if mus[fi][mi] > max_mus:
                label = mi
                max_mus = mus[fi][mi]
        self.__inertia += np.linalg.norm(self._centroids[label] - feature)
        return label


    def __update_centroids(self, features, mus, fuzzyness):
        for ci in range(0, len(self._centroids)):
            denominator = np.zeros(len(features[0]))
            numerator = 0.0
            for fi in range(0, len(features)):
                fuzzyfied = mus[fi][ci]**fuzzyness
                denominator += fuzzyfied * features[fi]
                numerator += fuzzyfied
            self._centroids[ci] = denominator / numerator
            print(len(self._centroids[0]))

    @staticmethod
    def __get_dists(features, centroids):
        dists = []
        for fi in range(0, len(features)):
            dists.append([])
            feature = features[fi]
            for ci in range(0, len(centroids)):
                centroid = centroids[ci]
                dists[fi].append(FuzzyKMeans.__calc_distance(feature, centroid))
        return dists


    @staticmethod
    def __calc_distance(feature, centroid):
        distance = 0.0
        centroid = centroid.ravel()
        for xi in range(0, len(feature)):
            distance += (feature[xi] - centroid[xi]) ** 2
        distance **= 0.5
        return distance


    @staticmethod
    def __get_mus(features, centroids, fuzzyness, dists):
        mus = []
        for fi in range(0, len(features)):
            mus.append([])
            same_centroids = []

            for ci in range(0, len(centroids)):
                sum = 0.0
                for ki in range(0, len(centroids)):
                    if (abs(dists[fi][ki]) < 1E-8):
                        same_centroids.append(ci)
                    else:
                        sum += (dists[fi][ci] / dists[fi][ki]) ** (2.0 / (fuzzyness - 1))

                if abs(sum < 1E-8):
                    sum = 1E-8 # naive smoothing
                mus[fi].append(1.0 / sum)

            if len(same_centroids) > 0:
                for ci in range(0, len(centroids)):
                    if ci in same_centroids:
                        mus[fi][ci] = 1.0 / len(same_centroids)
                    else:
                        mus[fi][ci] = 0.0
        return mus


    @staticmethod
    def __get_total_update_distance(old_centroids, new_centroids):
        total_distance = 0.0
        for ci in range(0, len(old_centroids)):
            total_distance += np.linalg.norm(old_centroids[ci]-new_centroids[ci])
        return total_distance


    def get_inertia(self):
        return self.__inertia


class Agglomerative(Clusterer):

    def __init__(self, components):
        self._components = components
        self.__delegate = AgglomerativeClustering(n_clusters=components)

    def cluster(self, features):
        return self.__delegate.fit_predict(features)

    def get_inertia(self):
        return 0


class DBSCAN(Clusterer):
    def __init__(self, eps=0.5):
        self.__delegate = sc.DBSCAN(eps=eps)

    def cluster(self, features):
        return self.__delegate.fit_predict(features)

    def get_inertia(self):
        return 0