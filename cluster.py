"""
Clusters the given set of images. Expects zero or one argument which is the path to configuration file.
Configuration file is a .json file which contains the following fields:
"feature-extraction" : name of the feature extraction model to use, supported models: "inception-v1", "inception-v2",
"inception-v3", inception-v4", "inception-resnet-v2", "resnet-v1-50", "resnet-v1-101" and "presaved". If "presaved"
is selected features will be loaded from a pickle file. All models are pre-trained on Imagenet dataset;
"use-finetuned" : whether to use fine tuned version of the model. Available only for inception-v2 model. The model
is fin tuned on caltech101 and micc101 datasets.
"reduction": method of dimension reduction, supported methods: "pca", "svd" and "none". If "none" is selected, no
dimension reduction will be applied;
"reduction-components" : reduced feature dimensions
"clustering" : clustering algorithm to use, supported algorithms: "kmeans", "fuzzy-kmeans", "agglomerative", "meanshift";
"clusters" : number of clusters to cluster images into. If "meanshift2 is selected as the clusterer, this is ignored;
"dbscan.eps" : max distance for two points to be considered neighbours in DBSCAN algorithm;
"fuzzy-kmeans.fuzzyness": the fuzzyness of the Fuzzy KMeans algorithm, the higher the value the higher the fuzzyness;
"save-features" : if true, features will be saved to a pickle file which can be reused later (useful when comparing
 different clusterers on same image set);
"save-reduction" : if true, reductions will be saved to a pickle file (useful when plotting clusters);
"save-clustering" : if true, clustering result will be saved to a pickle file (useful when post-processing results);
"results-dir-prefix" : prefix of the directory to save the results of clustering to
"dataset-dir" : directory of the dataset to cluster.
"presaved-file" : path to the file with presaved features on disk. Only available if "presaved" model is used.

If no config file is passed as an argument, the follwoing default values will be used instead:
"feature-extraction" : "inception-v2",
"reduction" : "svd",
"reduction-components" : 100,
"clustering" : "kmeans",
"clusters" : 10,
"dbscan.eps" : 0.5,
 "fuzzy-kmeans" : 2.0,
"save-features" : true,
"save-reduction" : true,
"save-clustering" : true,
"results-dir-prefix" : "./results",
"dataset-dir" : "../dataset".

The result of clustering will be saved to {results-dir-prefix}/clusters/c{cluster_number}.

"""

import feature_extraction
import preprocessing
import loader
import os
import sys
from shutil import copyfile, rmtree
from sklearn import decomposition
from sklearn.metrics.cluster import calinski_harabaz_score
from sklearn.metrics.cluster import silhouette_score
import clustering
import numpy as np
from util import save_to_file
from util import save_text_to_file

feature_extraction_mappings = {
    "inception-v1" : feature_extraction.InceptionV1,
    "inception-v2": feature_extraction.InceptionV2,
    "inception-v3": feature_extraction.InceptionV3,
    "inception-resnet-v2" : feature_extraction.InceptionResnetV2,
    "resnet-v1-50": feature_extraction.ResnetV1_50,
    "resnet-v1-101": feature_extraction.ResnetV1_101,
    "presaved": feature_extraction.PresavedFeatures
}


scale_image_width = {
    "inception-v1": 224,
    "inception-v2": 224,
    "inception-v3": 299,
    "inception-resnet-v2": 299,
    "resnet-v1-50": 224,
    "resnet-v1-101": 224,
    "presaved": 32
}

scale_image_height = {
    "inception-v1": 224,
    "inception-v2": 224,
    "inception-v3": 299,
    "inception-resnet-v2": 299,
    "resnet-v1-50": 224,
    "resnet-v1-101": 224,
    "presaved": 32
}

dimension_reduction_mappings = {
    "pca": decomposition.PCA,
    "svd": decomposition.TruncatedSVD,
    "none": None
}

clustering_mappings = {
    "kmeans": clustering.KMeans,
    "fuzzy-kmeans": clustering.FuzzyKMeans,
    "agglomerative": clustering.Agglomerative,
    "dbscan": clustering.DBSCAN
}

fixed_component_clusterers = ["kmeans", "fuzzy-kmeans", "agglomerative"]

configuration = {
    "feature-extraction" : "inception-v2",
    "use-finetuned" : False,
    "reduction" : "svd",
    "reduction-components" : 100,
    "clustering" : "kmeans",
    "clusters" : 10,
    "dbscan.eps" : 0.5,
    "fuzzy-kmeans.fuzzyness" : 2.0,
    "save-features" : True,
    "save-reductions" : True,
    "save-clustering" : True,
    "results-dir-prefix" : './results',
    "dataset-dir" : "../dataset",
    "dataset-name" : "dataset",
    "presaved-file" : None
}

import pickle
import json

def load_configuration(configfile):
    cfp = open(configfile, 'r')
    loaded = json.load(cfp)
    set_config("feature-extraction", loaded)
    set_config("use-finetuned", loaded)
    set_config("reduction", loaded)
    set_config("reduction-components", loaded)
    set_config("clustering", loaded)
    set_config("clusters", loaded)
    set_config("save-features", loaded)
    set_config("save-reductions", loaded)
    set_config("save-clustering", loaded)
    set_config("results-dir-prefix", loaded)
    set_config("dataset-dir", loaded)
    set_config("dataset-name", loaded)
    set_config("presaved-file", loaded)
    set_config("dbscan.eps", loaded)
    set_config("fuzzy-kmeans.fuzzyness", loaded)


def set_config(key, dict):
    global configuration
    if key in dict.keys():
        configuration[key] = dict[key]


def get_features(imgs, save_features, preprocessor):
    features = []
    pickle_features = {}
    fec = None
    count = 0
    if configuration["feature-extraction"] == "presaved":
        fec = feature_extraction_mappings[configuration["feature-extraction"]](configuration["presaved-file"])
    else:
        fec = feature_extraction_mappings[configuration["feature-extraction"]](use_finetuned=configuration["use-finetuned"])
    for img in imgs:
        img._bytes = preprocessor.preprocess(img._bytes) if img._bytes is not None else None
        feature = fec.extract_features(img)
        try:
            features.append(feature.ravel())
        except(AttributeError):
            try:
                feature = feature[0]
            except:
                feature = feature[0][0]
            features.append(feature.ravel())
        if save_features:
            pickle_features[img._filename] = feature.ravel()
        count += 1
        sys.stdout.write("\rComputing image features: %.2f%%" % (100.0 * count / len(imgs)))
    sys.stdout.write("\n")

    return features, pickle_features



def get_optimal_clusters(features, metric=silhouette_score):
    features = np.asarray(features)
    features_to_cluster = features[np.random.choice(features.shape[0], len(features) // 20, replace=False), :]
    start_n = len(features) // 100
    low_n = 2
    high_n = start_n * 2
    curr_n = start_n
    stop_condition = False
    old_setup = (low_n, curr_n, high_n)
    while not stop_condition:
        high_clusterer = clustering_mappings[configuration["clustering"]](high_n)
        low_clusterer = clustering_mappings[configuration["clustering"]](low_n)
        curr_clusterer = clustering_mappings[configuration["clustering"]](curr_n)
        high_score = metric(features_to_cluster, high_clusterer.cluster(features_to_cluster))
        low_score = metric(features_to_cluster, low_clusterer.cluster(features_to_cluster))
        curr_score = metric(features_to_cluster, curr_clusterer.cluster(features_to_cluster))

        print(str(low_n) + " " + str(curr_n) + " " + str(high_n))
        old_setup = (low_n, curr_n, high_n)
        if curr_score < high_score:
            low_n = curr_n
            curr_n = (high_n - curr_n) // 2 + curr_n
        elif curr_score < low_score:
            high_n = curr_n
            curr_n = (curr_n - low_n) // 2 + low_n
        else:
            low_n = (curr_n - low_n) // 2 + low_n
            high_n = (high_n - curr_n) // 2 + curr_n
        new_setup = (low_n, curr_n, high_n)
        stop_condition = new_setup[0] == old_setup[0] and new_setup[1] == old_setup[1] and new_setup[2] == old_setup[2]

    return curr_n


def main():
    argc = len(sys.argv)
    if (argc > 1):
        load_configuration(sys.argv[1])

    width = scale_image_width[configuration["feature-extraction"]]
    height = scale_image_height[configuration["feature-extraction"]]
    save_features = configuration["save-features"]
    save_reductions = configuration["save-reductions"]
    save_clustering = configuration["save-clustering"]

    results_dir = configuration["results-dir-prefix"]
    dataset_dir = configuration["dataset-dir"]

    if os.path.exists(results_dir):
        rmtree(results_dir)

    os.makedirs(results_dir)

    resizer = preprocessing.Resizer(width, height)
    scaler = preprocessing.Scaler()
    tf_reshaper = preprocessing.TensorFlowReshaper()
    preprocessors = [resizer, scaler, tf_reshaper]
    pipe = preprocessing.PipePreprocessor(preprocessors)

    print ("Loading images, please wait...")
    imgs = loader.load_as_rgb(dataset_dir, dont_load_bytes=configuration["feature-extraction"] == "presaved")
    print("Loaded " + str(len(imgs)) + " images.")
    features, pickle_features = get_features(imgs, save_features, pipe)

    if save_features:
        path = results_dir + "/" + "_".join([configuration["dataset-name"], configuration["feature-extraction"]])
        save_to_file(pickle_features, path, "Error! Could not export features.")

    dimred_cl = dimension_reduction_mappings[configuration["reduction"]]
    if dimred_cl is not None:
        dimred = dimred_cl(configuration["reduction-components"])
        features = dimred.fit_transform(features)

    features = [feature / np.linalg.norm(feature) for feature in features]

    if save_reductions:
        pickle_reductions = {}
        for ii in range(0, len(imgs)):
            img = imgs[ii]
            pickle_reductions[img._filename] = features[ii]
        path = results_dir + "/" + "_".join([configuration["dataset-name"], configuration["feature-extraction"], configuration["reduction"], str(configuration["reduction-components"])])
        save_to_file(pickle_reductions, path, "Error! Could not save reductions.")

    clusterer = None
    if configuration["clustering"] in fixed_component_clusterers:
        n_clusters = get_optimal_clusters(features) if configuration["clusters"] == "auto" else configuration["clusters"]
        if configuration["clustering"] == "fuzzy-kmeans":
            clusterer = clustering_mappings[configuration["clustering"]](n_clusters, configuration["fuzzy-kmeans.fuzzyness"])
        else:
            clusterer = clustering_mappings[configuration["clustering"]](n_clusters)
    else:
        if configuration["clustering"] == "dbscan":
            clusterer = clustering_mappings[configuration["clustering"]](eps=configuration["dbscan.eps"])
        else:
            clusterer = clustering_mappings[configuration["clustering"]]()

    print ("Clustering started...")
    clustered = clusterer.cluster(features)
    cluster_dict = {}
    for cli in range(0, len(imgs)):
        result = clustered[cli]
        if result in cluster_dict.keys():
            cluster_dict[result].append(imgs[cli])
        else:
            cluster_dict[result] = [imgs[cli]]

    print ("Clustering finished, results will be available shortly in following directories: ")

    dirs = [results_dir + "/clusters/c" + str(label) for label in cluster_dict.keys()]
    for dir in dirs:
        print (dir)
        os.makedirs(dir)

    for key in cluster_dict.keys():
        key_imgs = cluster_dict[key]
        for img in key_imgs:
            copyfile(img._path, results_dir + "/clusters/c" + str(key) + "/" + img._filename)

    if save_clustering:
        pickle_clustering = {}
        for ii in range(0, len(imgs)):
            img = imgs[ii]
            pickle_clustering[img._filename] = clustered[ii]
        path = results_dir + "/" + "_".join([configuration["dataset-name"], configuration["feature-extraction"], configuration["reduction"], str(configuration["reduction-components"]), configuration["clustering"]])
        save_to_file(pickle_clustering, path, "Error! Could not save clustering as pickle.")

    save_text_to_file(str(clusterer.get_inertia()), results_dir + str(clusterer.get_inertia()))
    print("All done!")


if __name__ == "__main__":
    main()