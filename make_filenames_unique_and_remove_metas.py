import os
import shutil
import sys

def make_unique_and_remove_metas(dir):
    cat = -1
    for folder, subs, files in os.walk(dir):
        cat += 1
        for filename in files:
            path = os.path.join(folder, filename)
            if filename.endswith('.meta'):
                os.remove(path)
            else:
                new_filename = str(cat) + "_" + filename
                new_path = os.path.join(folder, new_filename)
                shutil.move(path, new_path)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print ("Please provide path to folder to process.")
        exit(-1)
    make_unique_and_remove_metas(sys.argv[1])