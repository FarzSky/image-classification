import tensorflow as tf
slim = tf.contrib.slim
from lib.slim.nets.inception_v1 import *
from lib.slim.nets.inception_v2 import *
from lib.slim.nets.inception_v3 import *
from lib.slim.nets.inception_v4 import *
from lib.slim.nets.inception_resnet_v2 import *
from lib.slim.nets.resnet_v1 import *

import pickle
import os

models_dir = './models'
checkpoints_dir = "./models/checkpoints"
finetuned_dir = checkpoints_dir + "/finetuned"
finetuned_suffix = "-7511.data-00000-of-00001"

inc_resnet_v2 = None
resnet_v1_dict = None

class FeatureExtractor():

    def extract_features(self, img):
        raise NotImplementedError("Not implemented.")

class TensorFlowPreload(FeatureExtractor):

    def __init__(self, path, min_width, min_height, is_training = False, use_finetuned = True, finetuned_path = None):
        self.__path = checkpoints_dir + "/" + path
        if finetuned_path is not None:
            self.__finetuned_path = finetuned_dir + "/" + finetuned_path
        else:
            self.__finetuned_path = None
        if use_finetuned and self.__finetuned_path is None:
            print ("Fine-tuned model path not given, using non-fine.tuned model.")
            use_finetuned = False
        self._use_finetuned = use_finetuned
        self._width = min_width
        self._height = min_height
        self._is_training = is_training
        self._session = tf.Session()
        self._input = tf.placeholder(tf.float32, shape=[None, self._width, self._height, 3])
        self._arg_scope = None


    def extract_features(self, img):
        if self._arg_scope is None:
            self.__load()

        return self._extract(img._bytes)

    def __load(self):
        self._arg_scope = self._load_arg_scope()

        with slim.arg_scope(self._arg_scope):
            self._logits, self._end_points = self._prepare_vars()

        #.replace('\\', '/')
        if self._use_finetuned and self.__finetuned_path is not None:
            meta_path = TensorFlowPreload.__get_meta(self.__finetuned_path)
            init = tf.global_variables_initializer()
            self._session.run(init)
            saver = tf.train.import_meta_graph(meta_path)
            saver.restore(self._session, self.__finetuned_path)
        else:
            saver = tf.train.Saver()
            saver.restore(self._session, self.__path)


    @staticmethod
    def __get_meta(path):
        meta_path = path + ".meta"
        return meta_path


    def _extract(self, img):
        raise NotImplementedError("Not implemented.")


    def _load_arg_scope(self):
        raise NotImplementedError("Not implemented.")


    def _prepare_vars(self):
        raise NotImplementedError("Not implemented.")


class InceptionResnetV2(TensorFlowPreload):


    def __init__(self, is_training = False, use_finetuned=False):
        TensorFlowPreload.__init__(self, "inception_resnet_v2_2016_08_30.ckpt", 299, 299, is_training, use_finetuned=use_finetuned)

    def _extract(self, img):
        features, ignorable = self._session.run([self._end_points['PreLogitsFlatten'], self._logits], feed_dict={self._input: img})
        return features


    def _load_arg_scope(self):
        return inception_resnet_v2_arg_scope()


    def _prepare_vars(self):
        return inception_resnet_v2(inputs= self._input, is_training=self._is_training)


class ResnetV1(TensorFlowPreload):


    def __init__(self, flavor = 101, is_training=False, use_finetuned=False):
        ResnetV1.__check_flavor(flavor)
        TensorFlowPreload.__init__(self, "resnet_v1_" + str(flavor) + ".ckpt", 224, 224, is_training, use_finetuned=use_finetuned)
        self._flavor = flavor


    def _extract(self, img):
        features = self._session.run([self._logits], feed_dict={self._input: img})
        return features


    def _load_arg_scope(self):
        return resnet_arg_scope()


    def _prepare_vars(self):
        if self._flavor is 50:
            return resnet_v1_50(inputs=self._input, is_training=self._is_training)
        if self._flavor is 101:
            return resnet_v1_101(inputs=self._input, is_training=self._is_training)
        if self._flavor is 152:
            return resnet_v1_152(inputs=self._input, is_training=self._is_training)


    @staticmethod
    def __check_flavor(flavor):
        if flavor is not 50 and flavor is not 101 and flavor is not 152:
            raise ValueError ("Unsupported ResnetV1 flavor: " + str(flavor) + ".")

class ResnetV1_50(ResnetV1):
    def __init__(self, is_training=False, use_finetuned=False):
        ResnetV1.__init__(self, flavor = 50, is_training=is_training, use_finetuned=use_finetuned)


class ResnetV1_101(ResnetV1):
    def __init__(self, is_training=False, use_finetuned=False):
        ResnetV1.__init__(self, flavor = 101, is_training=is_training, use_finetuned=use_finetuned)


class ResnetV1_152(ResnetV1):
    def __init__(self, is_training=False, use_finetuned=False):
        ResnetV1.__init__(self, flavor = 152, is_training=is_training, use_finetuned=use_finetuned)


class InceptionV1(TensorFlowPreload):
    def __init__(self, is_training=False, use_finetuned=False):
        TensorFlowPreload.__init__(self, "inception_v1.ckpt", 224, 224, is_training, use_finetuned=use_finetuned)

    def _extract(self, img):
        features, ignorable = self._session.run([self._logits, self._end_points], feed_dict={self._input: img})
        return features

    def _load_arg_scope(self):
        return inception_v1_arg_scope()

    def _prepare_vars(self):
        return inception_v1(inputs=self._input, is_training=self._is_training, num_classes=1001)



class PresavedFeatures(FeatureExtractor):

    def __init__(self, feature_file):
        self.__feature_file = feature_file
        self._features = None

    def extract_features(self, img):
        if self._features is None:
            input = open(self.__feature_file, 'rb')
            self._features = pickle.load(input)
        return self._features[img._filename]


def inception_resnet_v2_extraction(img):
    global inc_resnet_v2
    if inc_resnet_v2 is None:
        inc_resnet_v2 = InceptionResnetV2()
    return inc_resnet_v2.extract_features(img)


def resnet_v1_extraction(img, flavor = 101):
    global resnet_v1_dict
    if resnet_v1_dict is None:
        resnet_v1_dict = {}
    if flavor not in resnet_v1_dict.keys():
        resnet_v1_dict[flavor] = ResnetV1(flavor=flavor)
    return resnet_v1_dict[flavor].extract_features(img)



class InceptionV2(TensorFlowPreload):
    def __init__(self, is_training=False, use_finetuned=False):
        TensorFlowPreload.__init__(self, "inception_v2.ckpt", 224, 224, is_training, use_finetuned=use_finetuned, finetuned_path="inception_v2.ckpt-7511")

    def _extract(self, img):
        features, ignorable = self._session.run([self._logits, self._end_points], feed_dict={self._input: img})
        return features

    def _load_arg_scope(self):
        return inception_v2_arg_scope()

    def _prepare_vars(self):
        return inception_v2(inputs=self._input, is_training=self._is_training, num_classes=1001)


class InceptionV3(TensorFlowPreload):
    def __init__(self, is_training=False, use_finetuned=False):
        TensorFlowPreload.__init__(self, "inception_v3.ckpt", 299, 299, is_training, use_finetuned=use_finetuned)

    def _extract(self, img):
        features, ignorable = self._session.run([self._logits, self._end_points], feed_dict={self._input: img})
        return features

    def _load_arg_scope(self):
        return inception_v3_arg_scope()

    def _prepare_vars(self):
        return inception_v3(inputs=self._input, is_training=self._is_training, num_classes=1001)



class PassThrough(FeatureExtractor):

    def extract_features(self, img):
        return img._bytes


class InceptionV4(TensorFlowPreload):
    def __init__(self, is_training=False, use_finetuned=False):
        TensorFlowPreload.__init__(self, "inception_v4.ckpt", 299, 299, is_training,use_finetuned=use_finetuned)

    def _extract(self, img):
        features, ignorable = self._session.run([self._logits, self._end_points], feed_dict={self._input: img})
        return features

    def _load_arg_scope(self):
        return inception_v4_arg_scope()

    def _prepare_vars(self):
        return inception_v4(inputs=self._input, is_training=self._is_training, num_classes=1001)