from scipy import misc
import util as ut

class Preprocessor():

    def preprocess(self, img):
        raise NotImplementedError("Not implemented.")

class Resizer(Preprocessor):

    def __init__(self, new_width, new_height):
        Resizer.__check_width_and_height(new_width, new_height)
        self._x = new_width
        self._y = new_height

    def preprocess(self, img):
        return misc.imresize(img, (self._x, self._y))

    def __check_width_and_height(new_width, new_height):
        if (new_width <= 0):
            raise ValueError("Invalid resizing: new width must be positive. " + ut.got(new_width))
        if (new_height <= 0):
            raise ValueError("Invalid resizing: new height must be positive. " + ut.got(new_height))



class Scaler(Preprocessor):

    def __init__(self, start = -1, end = 1, max_value = 255.0):
        Scaler.__check_start_end(start, end)
        Scaler.__check_max_value(max_value)
        self._start = start
        self._end = end
        self._max_value = max_value
        self._scale_value = (end - start)

    def preprocess(self, img):
        return self._scale_value * (img / self._max_value) + self._end

    @staticmethod
    def __check_start_end(start, end):
        if end <= start:
            raise ValueError("Invalid scaling: end of the range must be greater than start of the range." + ut.got((start, end)))

    @staticmethod
    def __check_max_value(max_value):
        if max_value == 0:
            raise ValueError("Invalid scaling: maximum value of the array can't be 0." + ut.got(max_value))

class TensorFlowReshaper(Preprocessor):

    def preprocess(self, img):
        return img.reshape(1, len(img), len(img[0]), 3)

class PipePreprocessor(Preprocessor):

    def __init__(self, preprocessors):
        self._preprocessors = preprocessors

    def preprocess(self, img):
        ret_val = img
        for preprocessor in self._preprocessors:
            ret_val = preprocessor.preprocess(ret_val)
        return ret_val
