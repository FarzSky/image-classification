import pickle

def got(value):
    return 'Got: ' + str(value)

def save_to_file(data, filename, error_msg):
    output = open(filename + ".pkl", 'wb')
    try:
        pickle.dump(data, output, protocol=pickle.HIGHEST_PROTOCOL)
    except:
        print(error_msg)

def save_text_to_file(data, filename):
    output = open(filename + ".txt", "w")
    try:
        output.write(data)
    except:
        pass
